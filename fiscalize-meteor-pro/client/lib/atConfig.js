AccountsTemplates.configure({
  defaultLayout: "layout",
  defaultLayoutRegions: {},
  defaultContentRegion: "main",
  confirmPassword: true,
  enablePasswordChange: true,
  forbidClientAccountCreation: false,
  overrideLoginErrors: true,
  sendVerificationEmail: false,
  lowercaseUsername: true,
  focusFirstInput: true,
  homeRoutePath: "/home",
  redirectTimeout: 5000,
  onLogoutHook() {
    FlowRouter.go("/");
  },
  texts: {
    title: {
      signIn: "Fiscalize PRO LOGIN"
    }
  }
});
