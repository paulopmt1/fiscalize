AccountsTemplates.configureRoute("signIn", {
  name: "signin",
  path: "/",
  template: "login",
});

FlowRouter.route("/home", {
  triggersEnter: [AccountsTemplates.ensureSignedIn],
  action() {
    BlazeLayout.render("home");
  }
});
