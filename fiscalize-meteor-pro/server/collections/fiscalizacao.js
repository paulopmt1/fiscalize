fiscalizacoes = new MysqlSubscription('allFiscalizacoes');

var liveDb = new LiveMysql({
  host: 'localhost',
  port: 3306,
  user: 'fiscalizepro',
  password: 'fiscalizepro',
  database: 'fiscalizepro'
});

var closeAndExit = function () {
  liveDb.end();
  process.exit();
};

// Close connections on hot code push
process.on('SIGTERM', closeAndExit);
// Close connections on exit (ctrl + c)
process.on('SIGINT', closeAndExit);

Meteor.publish('allFiscalizacoes', function () {
  return liveDb.select(
    'SELECT * FROM Fiscalizacao',
    [{table: 'Fiscalizacao'}]
  );
});
