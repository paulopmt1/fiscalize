SELECT notaFiscalId, Parlamentar.nome as parlamentar, Cota.nome as cota, Uf.sigla as uf, dataEmissao, descricao, descricaoSubCota, beneficiario, 
cpfCnpj, ano, mes, numeroDocumento, parcela, tipoDocumentoFiscal, nomePassageiro, trechoViagem, valor, valorGlosa, valorLiquido, dataInclusao
FROM NotaFiscal, Parlamentar, Uf, Cota 
WHERE
NotaFiscal.parlamentarId = Parlamentar.parlamentarId AND
NotaFiscal.ufId = Uf.ufId AND
NotaFiscal.cotaId = Cota.cotaId AND
NotaFiscal.notaFiscalId = 72320