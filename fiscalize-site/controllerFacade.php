		<?php
			// Tive alguns problemas em acessar os controllers diretamente. 
			// Criei esta "fachada" para facilitar as coisas. 
			// Não é necessária, porém acredito que deixa o código bem mais limpo. 
			include_once 'web_include.php';
			include_once INCLUDE_ROOT.'/controllers/FiscalizeController.class.php';		
			$fiscalizeController = new FiscalizeController();			
			session_start();
			if (isset($_GET['servico']))
			{				
				if($_GET['servico'] == "LOGIN")
				{					
					$_SESSION['USUARIOID'] = $_GET["id"];	
					$_SESSION['NOMEUSUARIO'] = $_GET["nome"];	
					$html = "{\"status\": \"1\"}";
				}				
				
				if($_GET['servico'] == "INSERIRANALISE")
				{	
					$html = $fiscalizeController->inserirAnalise($_GET["notaFiscalId"], 
																 $_GET["responsavelUsuarioId"],
																 $_GET["concluida"],
																 $_GET["comentarios"]);
				}					
				
				if($_GET['servico'] == "PARLAMENTAR")
				{				
					$html = $fiscalizeController->consultarParlamentares($_GET['id']);
				}												
				if($_GET['servico'] == "NOTAS")
				{				
					$html = $fiscalizeController->consultarNotaFiscal($_GET['id']);
				}												
				if($_GET['servico'] == "FISCALIZACOES")
				{				
					$html = $fiscalizeController->consultarFiscalizacoes($_GET['id']);
				}
				
				if($_GET['servico'] == "REATIVAR")
				{				
					$html = $fiscalizeController->excluiAnalise($_GET['notaFiscalId']);
				}				
				
				if($_GET['servico'] == "COTAS")
				{				
					$html = $fiscalizeController->consultarCotas();
				}													
				if($_GET['servico'] == "COTASP")
				{				
					$html = $fiscalizeController->consultarCotasParlamentar($_GET['id']);
				}													
				if($_GET['servico'] == "FISCALIZACOESP")
				{				
					$html = $fiscalizeController->consultarFiscalizacoesPorParlamentar($_GET['parlamentarId'],$_GET['cotaId']);
				}
				if($_GET['servico'] == "SAIR")
				{
					unset($_SESSION['USUARIOID'][1]);						
					$html = "{\"status\": \"1\"}";
				}				
			}
			echo $html;			
		?>
