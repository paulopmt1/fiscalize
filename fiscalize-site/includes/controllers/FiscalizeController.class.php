<?php
include_once INCLUDE_ROOT.'/daos/FiscalizeDAO.class.php';

class FiscalizeController {
	
	public function consultarCotasParlamentar($parlamentarId)
	{	  
		$strHTML = "";
		$fiscalizeDAO = new FiscalizeDAO();
		$lstParlamentares  = $fiscalizeDAO->consultarCotasParlamentar($parlamentarId);		
		$strHTML = json_encode($lstParlamentares);			
		return $strHTML;	  
	} 	
	
	public function inserirAnalise($notaFiscalId, $responsavelUsuarioId, $concluida, $comentarios)
	{	
		$fiscalizeDAO = new FiscalizeDAO();
		return $fiscalizeDAO->insereAnalise($notaFiscalId, $responsavelUsuarioId, $concluida, $comentarios);				
	}
	
	public function consultarFiscalizacoesPorParlamentar($parlamentarId,$cotaId)
	{	  
		$strHTML = "";
		$fiscalizeDAO = new FiscalizeDAO();
		$lstParlamentares  = $fiscalizeDAO->consultarFiscalizacoesPorParlamentar($parlamentarId,$cotaId);
		$strHTML = json_encode($lstParlamentares);			
		return $strHTML;	  
	} 	
	
	public function consultarNotaFiscal($notaFiscalId) {
		$strHTML = "";
		try {
			$fiscalizeDAO = new FiscalizeDAO();
			$notaFiscal = $fiscalizeDAO->consultarNotaFiscal($notaFiscalId);
			
			$dateEmissao = new DateTime($notaFiscal->dataEmissao);
			$dataEmissao = $dateEmissao->format('d/M/Y');
			
			$strHTML .= "<table class=\"table\">";
			
			$strHTML .= "<tr><th>ID da Nota Fiscal:</th><td>$notaFiscal->notaFiscalId</td></tr>";
			
			$strHTML .= "<tr><th>Parlamentar:</th><td><img src='http://104.131.229.175/fiscalize/parlamentar/$notaFiscal->ideCadastro.jpg'>$notaFiscal->parlamentar</td></tr>";
			$strHTML .= "<tr><th>Partido:</th><td><img src='http://104.131.229.175/fiscalize/partido/$notaFiscal->partido.gif'>$notaFiscal->partido</td></tr>";
			$strHTML .= "<tr><th>UF:</th><td>$notaFiscal->uf</td></tr>";

			$strHTML .= "<tr><th>Cota:</th><td>$notaFiscal->cota</td></tr>";
			$strHTML .= "<tr><th>Descrição:</th><td>$notaFiscal->descricao</td></tr>";
			$strHTML .= "<tr><th>Data Emissão:</th><td>$dataEmissao</td></tr>";
			
			$strHTML .= "<tr><th>Beneficiário:</th><td>$notaFiscal->beneficiario</td></tr>";
			$strHTML .= "<tr><th>CPF/CPNJ:</th><td>$notaFiscal->cpfCnpj</td></tr>";
			$strHTML .= "<tr><th>Mês/Ano:</th><td>$notaFiscal->mes/$notaFiscal->ano</td></tr>";
			$strHTML .= "<tr><th>Número Documento:</th><td>$notaFiscal->numeroDocumento</td></tr>";
			$strHTML .= "<tr><th>Nome Passageiro:</th><td>$notaFiscal->nomePassageiro</td></tr>";
			$strHTML .= "<tr><th>Trecho Viagem:</th><td>$notaFiscal->trechoViagem</td></tr>";
			$strHTML .= "<tr><th>Valor:</th><td>$notaFiscal->valor</td></tr>";
			
			$strHTML .= "</table>";
			
		} catch(NotaFiscalException $e) {
			$strHTML = $e->getMessage();
		}
		return $strHTML;
	}
	public function excluiAnalise($notaFiscalId)
	{
		$fiscalizeDAO = new FiscalizeDAO();
	    return 	$fiscalizeDAO->excluiAnalise($notaFiscalId);		
	}
	
	public function consultarFiscalizacoes($parlamentarId) {
		$strHTML = "";
		try {
			$fiscalizeDAO = new FiscalizeDAO();
			$fiscalizacoes = $fiscalizeDAO->consultarFiscalizacoes($parlamentarId);
			
			$strHTML = "<table class=\"table\">";
			$strHTML .= "<tr>";			
			$strHTML .= "<th>Suspeitas</th>";
			$strHTML .= "<th>Confiaveis</th>";
			$strHTML .= "<th>Total</th>";
			$strHTML .= "<th>Razão</th>";
			$strHTML .= "<th>Detalhes</th>";
			
			if(isset($_SESSION['USUARIOID']))
				$strHTML .= "<th>Concluir</th>";
			
			$strHTML .= "</tr>";						
			$emAnalise = 1;  
			
			foreach($fiscalizacoes as $fiscalizacao)
			{
				$strHTML .= "<tr>";				
				$strHTML .= "<td>$fiscalizacao->somaSuspeitas</td>";
				$strHTML .= "<td>$fiscalizacao->somaConfiaveis</td>";
				$strHTML .= "<td>$fiscalizacao->somaFiscalizacoes</td>";
				$strHTML .= "<td>$fiscalizacao->razaoConfiaveis</td>";				
				$strHTML .= "<td><button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" onclick=\"buscaDetalhes($fiscalizacao->notaFiscalId)\"  data-target=\"#myModal\">$fiscalizacao->notaFiscalId</button></td>"; 				
				if(isset($_SESSION['USUARIOID']))
				{
					if($fiscalizacao->analise == 0)
					{ 
						$strHTML .= "<td><button type=\"button\" class=\"btn btn-danger btn-lg\" data-toggle=\"modal\" onclick=\"formularioAnalise('Cézar Antônio',$fiscalizacao->notaFiscalId)\"  data-target=\"#modalAnalise\">Analisar</button></td>"; 								
					}else{
						$strHTML .= "<td><button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" onclick=\"reativarAnalise($fiscalizacao->notaFiscalId)\">Reativar ?</button></td>"; 								
					} 
				}					
				$strHTML .= "</tr>";
			}			
			$strHTML .= "</table>";			
		}catch(FiscalizacaoException $e){
			$strHTML = $e->getMessage();
		}
		return $strHTML;
	}			
	public function consultarCotas()
	{
		$strHTML = "";
		try {
			$fiscalizeDAO = new FiscalizeDAO();
			$lstCotas  = $fiscalizeDAO->consultarCotas();
			$strHTML = json_encode($lstCotas);			
		} catch(FiscalizacaoException $e) {
			$strHTML = $e->getMessage();
		}
		return $strHTML;
	}	
	
	public function consultarParlamentares($cotaId)
	{
		$strHTML = "";
		try {
			$fiscalizeDAO = new FiscalizeDAO();
			$lstParlamentares  = $fiscalizeDAO->consultarParlamentares($cotaId);
			$strHTML = json_encode($lstParlamentares);			
		} catch(FiscalizacaoException $e) {
			$strHTML = $e->getMessage();
		}
		return $strHTML;
	}
	
	public function numeroFiscalizacoes()
	{
		$strHTML = "";
		try{
			$fiscalizeDAO = new FiscalizeDAO();
			$contador = $fiscalizeDAO->numeroFiscalizacoes();
			
			$strHTML = "<table border='1'>";
			$strHTML .= "<tr>";
			$strHTML .= "<th>$contador</th>";
			$strHTML .= "</tr>";
			$strHTML .= "</table>";
			
		} catch(FiscalizacaoException $e){
			$strHTML = $e->getMessage();
		}
		return $strHTML;
	}	
}	
?>