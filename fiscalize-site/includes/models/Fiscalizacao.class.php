<?php
class Fiscalizacao 
{	
	public $notaFiscalId; 
	public $somaSuspeitas; 
	public $somaConfiaveis; 
	public $somaFiscalizacoes; 
	public $razaoConfiaveis;
	public $analise;
	
	public function popular($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis,$analise) {
		$this->notaFiscalId = $notaFiscalId;
		$this->somaSuspeitas = $somaSuspeitas;
		$this->somaConfiaveis = $somaConfiaveis;
		$this->somaFiscalizacoes = $somaFiscalizacoes;
		$this->razaoConfiaveis = $razaoConfiaveis;
		$this->analise = $analise;
	}	
	
	public function popularSemAnalise($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis) {
		$this->notaFiscalId = $notaFiscalId;
		$this->somaSuspeitas = $somaSuspeitas;
		$this->somaConfiaveis = $somaConfiaveis;
		$this->somaFiscalizacoes = $somaFiscalizacoes;
		$this->razaoConfiaveis = $razaoConfiaveis;		
	}		
}
?>