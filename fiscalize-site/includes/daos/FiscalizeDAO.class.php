<?php
include_once INCLUDE_ROOT.'/framework/base/BaseDAO.class.php';
include_once INCLUDE_ROOT.'/models/NotaFiscal.class.php';
include_once INCLUDE_ROOT.'/models/Fiscalizacao.class.php';
include_once INCLUDE_ROOT.'/models/Parlamentar.class.php';
include_once INCLUDE_ROOT.'/models/Cota.class.php';
include_once INCLUDE_ROOT.'/exceptions/NotaFiscalException.class.php';
include_once INCLUDE_ROOT.'/exceptions/FiscalizacaoException.class.php';

class FiscalizeDAO extends BaseDAO {
	
	public function __construct_conectado($conexao = null) {
		parent::__construct_conectado($conexao);
	}

	public function __construct() {
		parent::__construct();
	}
	
	public function insereAnalise($notaFiscalId, $responsavelUsuarioId, $concluida, $comentarios)
	{		
		$sql = "INSERT INTO Analise(notaFiscalId, responsavelUsuarioId, concluida, comentarios) VALUES (?,?,?,?)";
		if($statement = parent::preparar($sql))
		{
			try {
				$statement->bind_param('iiis',$notaFiscalId, $responsavelUsuarioId, $concluida, $comentarios);				
				$statement->execute();				
				$statement->free_result();
			} catch (Exception $e){
				throw new SQLException($e);
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return true; 		
	} 	
	
	public function consultarCotasParlamentar($parlamentarId)
	{
		$sql = " SELECT DISTINCT Cota.cotaId, Cota.nome"
			  ." FROM NotaFiscal"
			  ." INNER JOIN Cota ON Cota.cotaId = NotaFiscal.cotaId"
			  ." WHERE parlamentarId =?"; 
		$cotas = array();
		if($statement = parent::preparar($sql))
		{
			try {
				$statement->bind_param('i',$parlamentarId);				
				$statement->execute();
				$statement->bind_result($cotaId, $nome); 
				while ($statement->fetch()) {
					$cota = new Cota(); 					
					$cota->popular($cotaId, $nome);
					$cotas[] = $cota;
				}
				$statement->free_result();
			} catch (Exception $e){
				throw new SQLException($e);
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return $cotas;			
	} 	
	
	public function consultarCotas()
	{
		$sql = " SELECT cotaId,nome FROM Cota"; 				
		$cotas = array();
		if($statement = parent::preparar($sql))
		{
			try {
				$statement->execute();
				$statement->bind_result($cotaId, $nome); 
				while ($statement->fetch()) {
					$cota = new Cota(); 					
					$cota->popular($cotaId, $nome);
					$cotas[] = $cota;
				}
				$statement->free_result();
			} catch (Exception $e){
				throw new SQLException($e);
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return $cotas;			
	} 
	
	public function consultarParlamentares($cotaId)
	{
		$sql = " SELECT Parlamentar.parlamentarId,Parlamentar.nome" 
			  ." FROM NotaFiscal" 
			  ." INNER JOIN Cota ON Cota.CotaId = NotaFiscal.CotaId"
			  ." INNER JOIN Parlamentar ON Parlamentar.parlamentarId = NotaFiscal.parlamentarId" 
			  ." WHERE Cota.CotaId =?" 
			  ." GROUP BY Parlamentar.parlamentarId, Parlamentar.nome" 
			  ." ORDER BY nome"; 				
		$parlamentares = array();
		if($statement = parent::preparar($sql))
		{
			try {				
				$statement->bind_param('d',$cotaId);				
				$statement->execute();
				$statement->bind_result($parlamentarId, $nome); 
				while ($statement->fetch()){
					$parlamentar = new Parlamentar(); 					
					$parlamentar->popular($parlamentarId, $nome);
					$parlamentares[] = $parlamentar;
				}
				$statement->free_result();
			} catch (Exception $e){
				throw new SQLException($e);
			}
		}else{
			throw new SQLException($sql);
		}
		$statement->close();
		return $parlamentares;	
	} 		
	public function consultarNotaFiscal($notaFiscalId)
	{		
		$sql = " SELECT notaFiscalId, Partido.sigla AS partido, Parlamentar.nome AS parlamentar, Parlamentar.ideCadastro AS ideCadastro, Cota.nome AS cota," 
			   ." Uf.sigla AS uf, dataEmissao, descricao, descricaoSubCota, beneficiario, cpfCnpj, ano, mes, numeroDocumento, parcela, tipoDocumentoFiscal," 
			   ." nomePassageiro, trechoViagem, valor, valorGlosa, valorLiquido, dataInclusao" 
			   ." FROM NotaFiscal, Partido, Parlamentar, Uf, Cota"
			   ." WHERE NotaFiscal.parlamentarId = Parlamentar.parlamentarId"
			   ." AND NotaFiscal.ufId = Uf.ufId"
			   ." AND NotaFiscal.cotaId = Cota.cotaId"
			   ." AND Parlamentar.partidoId = Partido.partidoId" 
			   ." AND notaFiscalId =?"; 		
		if($statement = parent::preparar($sql)){
			try {								
				$statement->bind_param('i',$notaFiscalId);				
				$statement->execute();
				$statement->bind_result($notaFiscalId, $partido, $parlamentar, $ideCadastro, $cota, $uf, $dataEmissao, $descricao, $descricaoSubCota, $beneficiario, $cpfCnpj, $ano, $mes, $numeroDocumento,
							$parcela, $tipoDocumentoFiscal, $nomePassageiro, $trechoViagem, $valor, $valorGlosa, $valorLiquido, $dataInclusao);
				
				if ($statement->fetch()) {
					$notaFiscal = new NotaFiscal();
					$notaFiscal->popular($notaFiscalId, $partido, $parlamentar, $ideCadastro, $cota, $uf, $dataEmissao, $descricao, $descricaoSubCota, $beneficiario, $cpfCnpj, $ano, $mes, $numeroDocumento,
							$parcela, $tipoDocumentoFiscal, $nomePassageiro, $trechoViagem, $valor, $valorGlosa, $valorLiquido, $dataInclusao);
				} else {
					throw new NenhumaNotaFiscalEncontradaException();
				}
				$statement->free_result();
			} catch (Exception $e) {
				throw new ConsultarNotasFiscaisException();
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		
		return $notaFiscal;
	}

	public function consultarFiscalizacoesPorParlamentar($parlamentarId,$cotaId)
	{		
		$sql = " SELECT suspeitas.notaFiscalId as notaFiscalId,IFNULL(suspeitas.soma,0) as somaSuspeitas,IFNULL(confiaveis.soma,0) as somaConfiaveis, "
			  ." IFNULL(suspeitas.soma,0) + IFNULL(confiaveis.soma,0) as somaFiscalizacoes, IFNULL(suspeitas.soma/confiaveis.soma,0) as razaoConfiaveis,"
			  ." IFNULL((SELECT analiseId FROM Analise WHERE  notaFiscalId = 19636),0) AS Analise" 		
			  ." FROM (SELECT COUNT(*) as soma,NotaFiscal.notaFiscalId,parlamentarId,NotaFiscal.cotaId"  
			  ." FROM NotaFiscal LEFT JOIN Suspeita ON NotaFiscal.notaFiscalId=Suspeita.notaFiscalId" 
			  ." WHERE suspeita=true GROUP BY notaFiscalId,parlamentarId) as suspeitas" 
			  ." LEFT JOIN (SELECT COUNT(*) as soma, NotaFiscal.notaFiscalId,parlamentarId,NotaFiscal.cotaId"
			  ." FROM NotaFiscal LEFT JOIN Suspeita ON NotaFiscal.notaFiscalId=Suspeita.notaFiscalId" 
			  ." WHERE suspeita=false GROUP BY notaFiscalId,parlamentarId) as confiaveis ON suspeitas.notaFiscalId = confiaveis.notaFiscalId"			
			  ." WHERE IFNULL(suspeitas.parlamentarId,confiaveis.parlamentarId) =?" 
			  ." AND IFNULL(suspeitas.cotaId,confiaveis.cotaId) =?"  
			  ." ORDER BY razaoConfiaveis DESC, somaFiscalizacoes DESC";		 		
		$fiscalizacoes = array();
		if($statement = parent::preparar($sql)) {
			try {
				$statement->bind_param('ii', $parlamentarId,$cotaId);				
				$statement->execute();
				$statement->bind_result($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis,$Analise); 
				while ($statement->fetch()) {
					$fiscalizacao = new Fiscalizacao();
					$fiscalizacao->popular($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis,$Analise);
					$fiscalizacoes[] = $fiscalizacao;
				}
				$statement->free_result();
			} catch (Exception $e) {
				throw new ConsultarNotasFiscaisException();
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return $fiscalizacoes;
	}
	
	public function excluiAnalise($notaFiscalId)
	{
		$sql =  " DELETE FROM Analise where notafiscalId = ?";
		if($statement = parent::preparar($sql)) {
			try {
				$statement->bind_param('i', $notaFiscalId);				
				$statement->execute();				
				$statement->free_result();
			} catch (Exception $e) {
				throw new ConsultarNotasFiscaisException();
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return true;
	} 
	
	public function consultarFiscalizacoes($parlamentarId){
		
		$sql =  " SELECT suspeitas.notaFiscalId as notaFiscalId,IFNULL(suspeitas.soma,0) as somaSuspeitas,IFNULL(confiaveis.soma,0) as somaConfiaveis,"  
			   ." IFNULL(suspeitas.soma,0) + IFNULL(confiaveis.soma,0) as somaFiscalizacoes, IFNULL(suspeitas.soma/confiaveis.soma,0) as razaoConfiaveis,"			    
			   ." IFNULL((SELECT analiseId FROM Analise WHERE  notaFiscalId = IFNULL(suspeitas.notaFiscalId,confiaveis.notaFiscalId)),0) AS analise"
		       ." FROM  (SELECT COUNT(*) as soma, NotaFiscal.notaFiscalId,parlamentarId FROM NotaFiscal LEFT JOIN Suspeita ON NotaFiscal.notaFiscalId=Suspeita.notaFiscalId WHERE suspeita=true  GROUP BY notaFiscalId,parlamentarId) as suspeitas"
			   ." LEFT JOIN  (SELECT COUNT(*) as soma, NotaFiscal.notaFiscalId,parlamentarId FROM NotaFiscal LEFT JOIN Suspeita ON NotaFiscal.notaFiscalId=Suspeita.notaFiscalId WHERE suspeita=false GROUP BY notaFiscalId,parlamentarId) as confiaveis"
			   ." ON suspeitas.notaFiscalId = confiaveis.notaFiscalId"  
			   ." WHERE	IFNULL(suspeitas.parlamentarId,confiaveis.parlamentarId) =?"
			   ." ORDER BY razaoConfiaveis DESC, somaFiscalizacoes DESC, notaFiscalId  LIMIT 50";
		
		$fiscalizacoes = array();
		if($statement = parent::preparar($sql)) {
			try {
				$statement->bind_param('i', $parlamentarId);				
				$statement->execute();
				$statement->bind_result($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis,$analise); 
				while ($statement->fetch()) {
					$fiscalizacao = new Fiscalizacao();
					$fiscalizacao->popular($notaFiscalId, $somaSuspeitas, $somaConfiaveis, $somaFiscalizacoes, $razaoConfiaveis,$analise);
					$fiscalizacoes[] = $fiscalizacao;
				}
				$statement->free_result();
			} catch (Exception $e) {
				throw new ConsultarNotasFiscaisException();
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		return $fiscalizacoes;
	}
	
	public function numeroFiscalizacoes() {
		
		$sql = "SELECT COUNT(*) FROM fiscalize.Suspeita";
		
		if($statement = parent::preparar($sql)) {
			try {
				$statement->execute();
				$statement->bind_result($contador);
				
				if (!$statement->fetch()) {
					throw new NenhumaFiscalizacaoEncontradaException();
				}
				$statement->free_result();
			} catch (Exception $e) {
				throw new ConsultarFiscalizacoesException();
			}
		} else {
			throw new SQLException($sql);
		}
		$statement->close();
		
		return $contador;
	}	
}
?>