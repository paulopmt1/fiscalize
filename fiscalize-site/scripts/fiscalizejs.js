		var enderecoServico = 'controllerFacade.php';
		function acenderLuzes()
		{
			document.getElementById('luzApagada').style.display = 'none';
		}
		/** Mostra mensagem "Carregando".....*/
		function apagarLuzes()
		{
			document.getElementById('luzAcessa').style.display = 'block';
			document.getElementById('luzApagada').style.display = 'block';
		}		
		function buscaPar($cotaId,$nomeCota)
		{			
			$('#cotaConsultada').text($nomeCota);
			$('#NomeParlamentar').text('');
			$('#subMenu li').each(function (i, row)
			{
				var $row = $(row);				
				{
					$row.remove();
				}
			});
			var paramProducao = { blocking: false, format: 'json' };
			paramProducao['servico'] = 'PARLAMENTAR';			
			paramProducao['id'] = $cotaId;			
			$.ajax(
			{				
				url: enderecoServico,data: paramProducao, error: erroCarga, crossDomain: true,
				beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();},
				success: carregaPar
			});
		} 
		function cadastraAnalise()
		{
			var paramProducao = { blocking: false};
			paramProducao['servico'] = 'INSERIRANALISE';	
			paramProducao['notaFiscalId'] = $('#lblNota').text();	
			paramProducao['responsavelUsuarioId'] = 1;
			paramProducao['concluida']	= 1;
			paramProducao['comentarios'] = $('#txtComentario').val();						
			$.ajax(
			{
				url: enderecoServico,
				beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();},data: paramProducao, 				
				error: erroCarga, crossDomain: true,
				success: analiseCadastrada
			});				
		} 		
		function escondeFiscalizacoes()
		{
			$('#modalAnalise').modal('hide');					
			$('#tbNotas tr').each(function (i, row)
			{
				var $row = $(row);				
				{
					$row.remove();
				}
			});
			$('#cotaConsultada').text('');
			$('#NomeParlamentar').text('');				
		} 
		
		function analiseCadastrada(data)
		{
			alert("Nota Fiscal analisada com sucesso!");
			escondeFiscalizacoes();		
		} 		
		function erroCarga(data)
		{
			var jsonData = eval(data);
			alert(jsonData.valorProducao);
		}		
		function buscaNotas($id,$nome)
		{			
			$('#NomeParlamentar').text($nome);
			$('#tbNotas tr').each(function (i, row)
			{
				var $row = $(row);				
				{
					$row.remove();
				}
			});
			var paramProducao = { blocking: false};
			paramProducao['servico'] = 'FISCALIZACOES';	
			paramProducao['id'] = $id;	
			$.ajax(
			{
				url: enderecoServico,
				beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();}, data: paramProducao, 
				error: erroCarga, crossDomain: true,
				success: carregaNotas
			});
		}
		function buscaCotas()
		{		
			var paramProducao = { blocking: false};
			paramProducao['servico'] = 'COTAS';							
			$.ajax(
			{
				url: enderecoServico,
				beforeSend: function(){ apagarLuzes();},complete:function(){acenderLuzes();},data: paramProducao, 				
				error: erroCarga, crossDomain: true,
				success: carregaCotas
			});				
		} 		 
		function buscaDetalhes($id)
		{
			$('#tbDetalheNotas tr').each(function (i, row)
			{
				var $row = $(row);				
				{
					$row.remove();
				}
			});
			var paramProducao = { blocking: false};
			paramProducao['servico'] = 'NOTAS';	
			paramProducao['id'] = $id;				
			$.ajax(
			{
				url: enderecoServico,
				beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();},data: paramProducao, 				
				error: erroCarga, crossDomain: true,
				success: carregaDetalhes
			});
		} 
		function formularioAnalise(nomeUsuario,NotaFiscalId)
		{
			$('#tbAnaliseNotas tr').each(function (i, row)
			{
				var $row = $(row);				
				{
					$row.remove();
				}
			});						
			$("#lblNota").text(NotaFiscalId); 
			$("#lblUsuario").text(nomeUsuario); 
			$("textarea#txtComentario").val('');		
		} 	
		function reativarAnalise(notaFiscal)
		{
			if (confirm("Deseja reativar a nota fiscal:" + notaFiscal + '?'))
			{
				var paramProducao = { blocking: false};
				paramProducao['servico'] = 'REATIVAR';	
				paramProducao['notaFiscalId'] = notaFiscal;				
				$.ajax(
				{
					url: enderecoServico,
					beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();},data: paramProducao, 				
					error: erroCarga, crossDomain: true,
					success: notaFiscalReativada
				});								 
			}
		} 		
		function notaFiscalReativada()
		{
			alert('Nota fiscal reativada :)');	
			escondeFiscalizacoes();			
		} 		
		function carregaPar(data)
		{				
			var jsonRetorno = JSON.parse(data);			
			$.each(jsonRetorno, function (key, value)
            {                                
                $("#subMenu").append("<li><a href=\"#\" onclick=\"buscaNotas("+ value.parlamentarId +",'"+ value.nome +"')\">"+ value.nome +"</a></li>"); 										
            });						
		}
		function carregaCotas(data)
		{
			var jsonRetorno = JSON.parse(data);			
			$.each(jsonRetorno, function (key, value)
            {                                
                $("#subMenuCotas").append("<li><a href=\"#\" onclick=\"buscaPar("+ value.cotaId +",'"+ value.nome +"')\">"+ value.nome +"</a></li>"); 										
            });						
		}		
		function carregaDetalhes(data)
		{		    
			$("#tbDetalheNotas").append(data); 			
		} 
		function carregaNotas(data)
		{						
			$("#tbNotas").append(data); 
		} 
		// Autenticação do usuário
		function autenticaUsuario(usuarioId,usuarioNome)
		{
			var paramProducao = { blocking: false};
			paramProducao['id'] = usuarioId;	
			paramProducao['nome'] = usuarioNome;							
			paramProducao['servico'] = 'LOGIN';	
			$.ajax(
			{
				url: enderecoServico,
				beforeSend:function(){ apagarLuzes();},complete:function(){acenderLuzes();},data: paramProducao, 				
				error: erroCarga, crossDomain: true,
				success: respostaAutenticacao
			});
		} 		
		function respostaAutenticacao(data)
		{
			var jsonRetorno = JSON.parse(data);			
			if(jsonRetorno.status == 1)
			{
				window.location = 'index.html';
			}  			
		} 